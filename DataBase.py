from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from datetime import datetime

app = Flask(__name__,template_folder='./templates')

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///flask_notepad.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = 'flask_notepad_key'

db = SQLAlchemy()
db.init_app(app)

#user information tables
class user_information(db.Model):
    id = db.Column(db.Integer, primary_key=True,unique=True)
    username = db.Column(db.String(50), nullable=False)
    phone_number = db.Column(db.Integer,unique=True,nullable=False)
    email = db.Column(db.String(120), nullable=False)
    password = db.Column(db.String(120),nullable=False)


#user note tables
class user_notes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), nullable=False)
    title = db.Column(db.String(128), nullable=False)
    notes = db.Column(db.String(1024), nullable=False)
    date = db.Column(db.String(128))

    

#user interface tables
class user_interface(db.Model):
    id = db.Column(db.Integer, primary_key=True,unique=True)
    username = db.Column(db.String(50), nullable=False,unique=True)
    email_verify = db.Column(db.Integer)
    theme = db.Column(db.Integer)



# def read_photo_file(file_path):
#     with open(file_path, 'rb') as file:
#         photo_data = file.read()
#     return photo_data



# #user interface tables
# class user_notes(db.Model):
#     id = db.Column(db.Integer, primary_key=True,unique=True)
#     username = db.Column(db.String(50), nullable=False,unique=True)
#     light_mode = db.Column(db.Integer,nullable=False)
#     photo = db.Column(db.LargeBinary)
    


