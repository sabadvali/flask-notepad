from flask import Flask,session
from DataBase import *
import re
import random
from datetime import datetime
from email.message import EmailMessage
import ssl
import smtplib
import os, requests, datetime, re, time, json, pickle




log_filename = "log_files"
script_path = os.path.dirname(os.path.realpath(__file__))
printing = True
write_in_log = True
def print_and_log(message, empty_line=False):
    log_file_path = script_path + "/" + log_filename
    pid  = os.getpid()
    if printing is True:
        print("[" + datetime.datetime.now().strftime('%Y-%m-%d %T') + "] [" + str(pid) + "] " + message)
    if write_in_log is True:
        with open(log_file_path, "a") as log_file:
            if not empty_line:
                log_file.write("[" + datetime.datetime.now().strftime('%Y-%m-%d %T') + "] [" + str(pid) + "] " + message + "\n")
            else:
                log_file.write("\n")






def check_user_information(username,phone,email,reg_password,reg_retype_password):
    checked_password = check_password(reg_password,reg_retype_password)
    if checked_password == 1:
        checked_uname_phone_email = check_uname_phone_email(username,phone)
        if checked_uname_phone_email == 1:
            checked_table = check_table(username,phone,email)
            if checked_table == 1:
                return 1
            else:
                return checked_table
        else:
            return checked_uname_phone_email
   
    else:
        return checked_password
    

def check_password(pswrd,retype_pswrd):
    if len(pswrd) < 8:
        return "მითითებული პაროლი მინიმუმ 8 სიმბოლოსგან უნდა შედგებოდეს."
    else:
        for i in pswrd:
            if i == "~" or i == "@" or i == "#" or i == "$" or i == "%" or i == "^" or i == "&" or i == "*" or i == "(" or i == ")" or i == " ":
                return "გთხოვთ პაროლის ველში არ გამოიყენოთ სპეციალური სიმბოლოები: “~ ! @ # $ % ^ & * ( )"
        if pswrd == retype_pswrd:
            return 1
        elif pswrd != retype_pswrd:
            return "პაროლები არ ემთხვევა ერთმანეთს"


def check_uname_phone_email(username,phone):
    if len(username) < 4 or len(username) > 20:
        return "თქვენი სახელი მინიმუმ 4 და მაქსიმუმ 20 სიმბოლოსგან უნდა შედგებოდეს."
    if len(phone) != 9:
        return "ტელეფონის ნომერი უნდა შედგებოდეს 9 სიმბოლოსგან"
    else:
        pnumber = has_special_symbols_or_letters(phone)
        if pnumber == 1:
            return 1
        else:
            return pnumber
            

def has_special_symbols_or_letters(phone_number):
    pattern = r'[^\d]'  

    if re.search(pattern, phone_number):
        return "თქვენი ნომრის ველი არუნდა შეიცავდეს სპეციალურ სიმბოლოებს ან ასოებს."
    else:
        return 1


def check_table(usname,phone,inputemail):
    nameoutput = db.session.query(user_information.query.filter_by(username=usname).exists()).scalar()
    if nameoutput:
         return "თქვენი სახელით უკვე არის რეგისტრირებული ცადეთ სხვა სახელი."
    
    phoneoutput = db.session.query(user_information.query.filter_by(phone_number=phone).exists()).scalar()
    if phoneoutput:
        return "მითითებული ნომერი რეგისტრირებულია ცადეთ სხვა ნომრით."
    
    emailoutput = db.session.query(user_information.query.filter_by(email=inputemail).exists()).scalar()
    if emailoutput:
        return "მითითებული იმეილი რეგისტრირებულია."
    
    return 1       
         
def random_6_digit_number():
    random_number = random.randint(100000, 999999)
    return random_number
    


def logincheck(usname,pswrd):

    login_result = db.session.query(user_information).filter(user_information.username == usname, user_information.password == pswrd).first()
    if login_result:
        return 1
    else:
        return "სახელი ან პაროლი არასწორია სცადეთ თავიდან."



def default_user(uname):
    default_interface = user_interface(
        username=uname,
        email_verify=1,
        theme = 1
        )

    db.session.add(default_interface)
    db.session.commit()



def send_email(email,subject,message):
    email_sender = 'flasknotepad@gmail.com'
    email_password = 'vscvjzbzvzgillar'
    
    session['email_reciver'] = email
    session['subject'] = subject
    session['body'] = message

    em = EmailMessage()

    em['From'] = email_sender
    em['To'] = session.get('email_reciver')
    em['subject'] = session.get('subject')
    em.set_content(session.get('body'))

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL('smtp.gmail.com', 465, context=context) as smtp:
        smtp.login(email_sender, email_password)
        smtp.sendmail(email_sender,session.get('email_reciver'), em.as_string())

    session.pop('email_reciver',None)
    session.pop('subject',None)
    session.pop('body',None)









# def send_sms_via_magti(message, numbers='', try_number=1, time_until_next_try=1, request_timeout=1, time_until_tracking_sms=5):

#     sent_message_ids_with_numbers = dict()
#     failed_numbers = []
#     print_and_log('იწყება მაგთის ლინკით sms-ების დაგზავნა')
#     for try_index in range(1, try_number+1):
#         for number in numbers:
#             params = {'username': magti_username,
#                       'password': magti_password,
#                       'client_id': 643,
#                       'service_id': 1,
#                       'to': number,
#                       'text': message,
#                       'coding': 2}
#             try:
#                 response = requests.get(magti_url, timeout=request_timeout, params=params)
#                 if not response.ok:
#                     failed_numbers.append(number)
#                     print_and_log("მითითებულ ნომერზე'{}' ვერ გაიგზავნა sms-ი. status_code = '{}', try_index = {}".format(number, response.status_code, try_index))
#                     continue
#                 else:
#                     status = response.text[:4]
#                     if status != '0000':
#                         failed_numbers.append(number)
#                         print_and_log("მითითებულ ნომერზე'{}' ვერ გაიგზავნა sms-ი. response_text = '{}', try_index = {}".format(number, response.text, try_index))
#                         continue
#                     else:
#                         sent_message_ids_with_numbers[number] = response.text.split('-')[1].strip()
#             except Exception as ex:
#                 failed_numbers.append(number)
#                 print_and_log("მითითებულ ნომერზე'{}' ვერ გაიგზავნა sms-ი. try_index = {}.\n{} ".format(number,  try_index, str(ex)))

#             if sent_message_ids_with_numbers:
#                 time.sleep(time_until_tracking_sms)
#                 for number, message_id in sent_message_ids_with_numbers.items():
#                     track_url_params = {'username': magti_username,
#                                         'password': magti_password,
#                                         'client_id': 643,
#                                         'service_id': 1,
#                                         'message_id': message_id
#                                     }
#                     track_response = requests.get(magti_track_url, timeout=request_timeout, params=track_url_params)
#             if not track_response.ok:
#                 failed_numbers.append(number)
#                 print_and_log("მითითებულ ნომერზე '{}' ვერ  მოხერხდა sms სტატუსის შემოწმება")
#             else:
#                 if track_response.text.strip() in ['1','4','8'] :
#                     print_and_log("მითითებულ ნომერზე '{}' sms-ი წარმატებით გაიგზავნა მაგთის ლინკით. track_response_text = '{}', try_index = {}".format(number, track_response.text.strip(), try_index))
#                 else:
#                     failed_numbers.append(number)
#                     print_and_log("მითითებულ ნომერზე'{}' ვერ გაიგზავნა sms-ი. track_response_text = '{}', try_index = {}".format(number, track_response.text.strip(), try_index))
#         if not failed_numbers:
#             print_and_log("sms-ები დაიგზავნა წარმატებით. try_index = {}".format(try_index))
#             break
#         else:
#             print_and_log("sms-ების დაგზავნა ვერ მოხერხდა {} ნომერთან. try_index = {}".format(len(failed_numbers), try_index))
#             # numbers = failed_numbers.copy()
#             numbers = list(failed_numbers)
#             if try_index != try_number:
#                 failed_numbers = []
#                 time.sleep(time_until_next_try)
#     print_and_log('დასრულდა მაგთის ლინკით sms-ების დაგზავნა')
#     return failed_numbers


