from flask import Flask,render_template,redirect,url_for,request,flash,session,jsonify
from flask_sqlalchemy import SQLAlchemy
import os
import urllib.request
from Functions import Function
from DataBase import *




@app.route("/",methods=["GET","POST"])
def main():

    session['access'] = False
    session['verify_access'] = False

                   
    if request.method == "POST":

        if "register_btn" in request.form:
            reg_username = request.form['reg_container_username']
            reg_phone_number = request.form['reg_container_phone']
            reg_email = request.form['reg_container_email']
            reg_password = str(request.form['reg_container_password'])
            reg_retype_password = request.form['reg_container_retypepassword']
            if len(reg_username) == 0 or len(reg_phone_number) == 0 or len(reg_email) == 0 or len(reg_password) == 0 or len(reg_retype_password) == 0:
               flash("გთხოვთ შეავსოთ ყველა ველი.")
               return render_template("main.html",
                                           reg_username=reg_username,
                                           reg_phone_number=reg_phone_number,
                                           reg_email=reg_email,
                                           reg_password=reg_password,
                                           reg_retype_password=reg_retype_password)
            else:
                reg_output = Function.check_user_information(reg_username,reg_phone_number,reg_email,reg_password,reg_retype_password)
                if reg_output == 1:
                    session['reg_username'] = reg_username
                    session['reg_phone_number'] = reg_phone_number
                    session['reg_email'] = reg_email
                    session['reg_password'] = reg_password
                    session['verify_access'] = True
                    session['access'] = False
                    
                    session['verify_error_count'] = 0

                    return redirect(url_for("verify"))
                else:
                    flash(str(reg_output))
                    return render_template("main.html",
                                           reg_username=reg_username,
                                           reg_phone_number=reg_phone_number,
                                           reg_email=reg_email,
                                           reg_password=reg_password,
                                           reg_retype_password=reg_retype_password)

                  
        elif "logincon_loginbtn" in request.form:
            log_password = request.form["login_container_password"]
            log_username = request.form["login_container_username"]
            if len(log_username) == 0 or len(log_password) == 0:
                flash("გთხოვთ შეავსოთ ყველა ველი.")
            else:
                log_output = Function.logincheck(log_username,log_password)
                if log_output == 1:
                    session['uname'] = log_username
                    usr = db.session.query(user_information).filter(user_information.username == log_username).first()
                    session['reg_email'] = usr.email
                    session['access'] = True
                    print(session.get('access'))
                    print(session.get('reg_email'))
                    return redirect(url_for("flask_notepad"))
                else:
                    flash(str(log_output))
                    return render_template("main.html",log_username=log_username,log_password=log_password)
                
    return render_template("main.html")



@app.route('/flask_notepad',methods=["POST","GET"])
def flask_notepad():   
    session['verify_access'] = False
    access = session.get('access')
    username = session.get('uname')
    session['reg_email']  = session.get('reg_email')

    print(username)

    if access == True:
        if request.method == "POST":
            if "add_button" in request.form:            
                input_note = request.form['note_input']
                input_title = request.form['title_input']
                if input_note and input_title:
                    current_utc_datetime = datetime.utcnow()
                    date_now = current_utc_datetime.date()  
                    add_note = user_notes(
                    username=username,
                    title=input_title,
                    notes=input_note,
                    date=date_now
                    )
                    db.session.add(add_note)
                    db.session.commit()
                    return redirect(url_for("flask_notepad"))         
                else:
                    flash("გთხოვთ შეავსოთ ყველა ველი.")
                    return redirect(url_for("flask_notepad"))
                
            if "theme-btn" in request.form:
                mode = user_interface.query.filter_by(username=username).first()
                theme_value = mode.theme
                if theme_value == 1:
                    mode.theme = 0
                    db.session.commit()
                    return redirect(url_for("flask_notepad"))
                elif theme_value == 0:
                    mode.theme = 1
                    db.session.commit()
                    return redirect(url_for("flask_notepad"))

            if "verify-btn" in request.form:
                print("shemovida")
                verify_value = user_interface.query.filter_by(username=username).first()
                verify_value = verify_value.email_verify
                if verify_value == 1:
                    return render_template("email_verify.html")
                elif verify_value == 0:
                    print("your email is verifyed")
                    return redirect(url_for("flask_notepad"))
                
            if "logout_btn" in request.form:
                session.pop('uname', None)
                username = None
                return redirect(url_for("main"))


        user_dashboard = user_interface.query.filter_by(username=username).first()
        if user_dashboard:
            email_verify = user_dashboard.email_verify
            theme = user_dashboard.theme

            # user_note = user_notes.query.filter_by(username=username).all()
            user_note= user_notes.query.filter_by(username=username).with_entities(user_notes.title, user_notes.notes, user_notes.date).all()

            note_list = [] 
            if user_note:
                for i in user_note:
                    note_list.append(i)  
            else:
                flash("თქვენ ჯერ არ გაქვთ შენახული წერილი.")
            return render_template ("main_board.html",
                                    username=username,
                                    notes=note_list,
                                    email_verify=email_verify,
                                    theme=theme)   
        else:
            return redirect(url_for("main"))
        
    elif access == False:
        return redirect(url_for("main"))


@app.route('/email_verify',methods=["POST","GET"])
def email_verify():
    if request.method == "POST":
        if "send_btn" in request.form:
            session['verifycode'] = Function.random_6_digit_number()
            session['email_message'] = f"A request has been recieved to get 6 digit code:\n '{session.get('verifycode')}'"
            Function.send_email(session.get('reg_email'),"Hello",session.get('email_message'))
            flash("ვერიფიკაციის კოდი გამოგზავნილია.\n გთხოვთ დაელოდოთ არაუმეტეს 2 წუთი. ბიჭო")

        if "verify_btn" in request.form:
            session['input_code'] = int(request.form['input_code'])
            if session.get('input_code') == session.get('verifycode'):
                username = session.get('uname')
                emlv = user_interface.query.filter_by(username=username).first()
                emlv.email_verify = 0
                db.session.commit()
                return redirect(url_for('flask_notepad'))
            else:
                flash("მითითებული კოდი არასწორია სცადეთ თავიდან.")
        if "back_btn" in request.form:
            return redirect(url_for('flask_notepad'))
        
    return render_template("email_verify.html")


@app.route('/verify',methods=["POST","GET"])
def verify():

    session['access'] = False
    verify_access = session.get('verify_access')

    if session.get('verify_error_count') == 3:
        session['verify_error_count'] = 0

        session.pop('_flashes', [])
        session.pop('reg_username', None)
        session.pop('reg_phone_number', None)
        session.pop('reg_email', None)
        session.pop('reg_password', None)
        return redirect(url_for("main"))
    
    if verify_access == True:
        if request.method == "POST":
            reg_username = session.get('reg_username')
            reg_phone_number = session.get('reg_phone_number')
            reg_email = session.get('reg_email')
            reg_password = session.get('reg_password')
            print(reg_username, reg_phone_number, reg_email, reg_password)

            if "verify_send_btn" in request.form:
                session['randomnumber'] = Function.random_6_digit_number()
                print(session['randomnumber'])

            if "verify_submit_btn" in request.form:
                input_number_value = request.form['input_verification_code']
                print(input_number_value)

                if len(input_number_value) != 6:
                    flash("ვერიფიკაციის კოდი უნდა შედგებოდეს 6 ციფრისგან.")
                    return render_template("verification.html",input_number_value=input_number_value)
                
                input_number_value = int(input_number_value)
                if input_number_value == session.get('randomnumber'):
                    session['randomnumber']
                    print(reg_username,reg_phone_number,reg_email,reg_password)

                    add_user = user_information(
                        username=reg_username,
                        phone_number=reg_phone_number,
                        email=reg_email,
                        password=reg_password
                    )

                    db.session.add(add_user)
                    db.session.commit()
                    
                    Function.default_user(reg_username)
                    session['access'] = True

                    session['verify_access'] = False
                    
                    session['verify_error_count'] = 0

                    session['uname'] = reg_username
                     

                    session.pop('_flashes', [])
                    session.pop('reg_username', None)
                    session.pop('reg_phone_number', None)
                    # session.pop('reg_email', None)
                    session.pop('reg_password', None)
                    session.pop('randomnumber', None) 
                    return redirect(url_for("flask_notepad"))
                
                elif input_number_value != session.get('randomnumber'):
                    flash("მითითებული 6 ვიფრიანი კოდი არასწორია სცადეთ თავიდან.")
                    session['verify_error_count'] += 1
                    return render_template("verification.html",input_number_value=input_number_value)
                
            if "verify_back_btn" in request.form:
                session.pop('reg_username', None)
                session.pop('reg_phone_number', None)
                session.pop('reg_email', None)
                session.pop('reg_password', None)
                session.pop['uname', None]
                session['verify_error_count'] = 0
                return redirect(url_for("main"))
            
        return render_template("verification.html")
    
    elif verify_access == False:
        return redirect(url_for("main"))



@app.route('/forgot_password',methods=['POST','GET'])
def forgot_password():
    session['access'] = False
    if request.method == "POST":
        selected_option = request.form['optionSelect']

        if selected_option == 'phone':
            if "forgot_submit_btn" in request.form:
                session['forgot_phone_number'] = request.form['phone'] 
                print(session['forgot_phone_number'] )     
                if len(session['forgot_phone_number']) != 9:
                    flash("მობილიურის ნომერი უნდა შედგებოდეს 9 ციფრისგან.")
                    session.pop('forgot_phone_number', None)
                    return render_template("forgotpassword.html")
                check_user_phone = user_information.query.filter_by(phone_number=session['forgot_phone_number']).all()
                if check_user_phone:
                    i_P = check_user_phone[0]
                    unm_P = i_P.username
                    psw_P = i_P.password
                    session['forgoten_username_P'] = unm_P
                    session['forgoten_password_P'] = psw_P
                    unm_P = None
                    psw_E = None
                    print(f"თქვენი სახელი და პაროლია: {session['forgoten_username_P']},{session['forgoten_password_P']} ")
                    session.pop('forgoten_username_P', None)
                    session.pop('forgoten_password_P', None)
                    return redirect(url_for('main'))
                else:
                    session.pop('forgot_phone_number', None)
                    flash("მითითებული ნომერი არარის რეგისტრირებული ")
                    return render_template("forgotpassword.html")
                
        elif selected_option == 'email':
            if "forgot_submit_btn" in request.form:
                session['forgot_email'] = request.form['email']
                if len(session['forgot_email']) == 0:
                    flash("გთხოვთ შეავსოთ ველი")
                    session.pop('forgot_email',None)
                    return render_template("forgotpassword.html")
                check_user_email = user_information.query.filter_by(email=session['forgot_email']).all()
                if check_user_email:
                    i_E = check_user_email[0]
                    unm_E = i_E.username
                    psw_E = i_E.password
                    session['forgoten_username_E'] = unm_E
                    session['forgoten_password_E'] = psw_E
                    unm_E = None
                    psw_E = None

                    session['email_message'] = f"A request has been recieved to get your username and password:\n Your UserName: '{session.get('forgoten_username_E')}'\n Your Password: '{session.get('forgoten_password_E')}'"
                    Function.send_email(session.get('forgot_email'),f"Hello {session.get('forgoten_username_E')}",session.get('email_message'))
                    
                    session.pop('forgoten_username_E',None)
                    session.pop('forgoten_password_E',None)
                    session.pop('email_message',None)
                    return redirect(url_for('main'))
                else:
                    session.pop('forgot_email',None)
                    flash("მითითებული იმეილი არარის რეგისტრირებული.")
                    return render_template("forgotpassword.html")

        elif selected_option != 'email' and selected_option != 'phone':
            flash("გთხოვთ მიუთითოთ რომელი გზით გნებავთ ექაუნთის აღდგენა")
            return render_template("forgotpassword.html")

    return render_template("forgotpassword.html")






@app.route('/test',methods=['POST','GET'])
def abc():

    theme=1

    return render_template("test2.html",username="dato",email_verify=0,notes={"hello this is first note from saba"},theme=theme)




if __name__ == "__main__":
    with app.app_context():
        db.create_all()

    app.run(debug=True,host='0.0.0.0',port=5555)












# from flask import Flask, redirect, render_template, request, session, url_for

# app = Flask(__name__,template_folder='./templates')
# app.secret_key = 'your_secret_key'

# @app.route('/login', methods=['GET', 'POST'])
# def login():
#     if request.method == 'POST':
#         username = request.form['username']
#         session['username'] = username
#         return redirect(url_for('home'))
    
#     return render_template('login.html')


# @app.route('/')
# def home():
#     username = session.get('username')
#     print(username)
#     if username:
#         return 'Hello, {}'.format(username)
#     else:
#         return 'You are not logged in.'

# if __name__ == '__main__':
#     app.run()
